﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogforum.Models
{
    public class ForumIndexModel
    {
        public IEnumerable<ForumListingModel> ForumListing { get; set; }
    }
}
