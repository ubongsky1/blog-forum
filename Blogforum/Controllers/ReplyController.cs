﻿using Blogforum.Data;
using Blogforum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogforum.Controllers
{
    [Authorize]
    public class ReplyController : Controller
    {
        private readonly IPost _postService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IReplyService _replyService;
        private readonly IApplicationUser _userService;

        public ReplyController(IPost postService,UserManager<ApplicationUser> userManager, IReplyService replyService,IApplicationUser userService)
        {
            _postService = postService;
            _userManager = userManager;
            _replyService = replyService;
            _userService = userService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Create(int id)
        {
            var post = _postService.GetById(id);

            var user = await _userManager.GetUserAsync(User);

            var model = new PostReplyModel()
            {
                 PostContent=post.Content,
                 PostTitle=post.Title,
                 PostId=post.Id,
                 AuthorId=user.Id,
                 AuthorName=user.UserName,
                 AuthorImageUrl=user.ProfileImageUrl,
                 AuthorRating=user.Rating,
                 IsAuthorAdmin=User.IsInRole("Admin"),
                 Created=DateTime.Now,
                 ForumName=post.Forum.Title,
                 ForumId=post.Forum.Id,
                 ForumImageUrl=post.Forum.ImageUrl
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddReply(PostReplyModel model)
        {
            var userid = _userManager.GetUserId(User);
            var s = Convert.ToInt32(userid);
            var user = await _userManager.FindByIdAsync(userid);

            var reply = BuildReply(model, user);
            await  _replyService.Create(reply);
            await _userService.UpdateUserRating(s, typeof(PostReply));

            return RedirectToAction("Index", "Post", new { id = reply.Post.Id });
           
        }

        private PostReply BuildReply(PostReplyModel model, ApplicationUser user)
        {
            var post = _postService.GetById(model.PostId);

            return new PostReply
            {
                Post = post,
                Content = model.ReplyContent,
                Created=DateTime.Now,
                User=user,

            };
        }
    }
}
