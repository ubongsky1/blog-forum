﻿using Blogforum.Data;
using Blogforum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Blogforum.Controllers
{
    public class ForumController : Controller
    {
        private readonly IForum _forumservice;
        private readonly IPost _postservice;
        private readonly HostingEnvironment _hostingEnvironment;

        public ForumController(IForum forumservice,IPost postservice, HostingEnvironment hostingEnvironment)
        {
            _forumservice = forumservice;
            _postservice = postservice;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {

            var forums = _forumservice.GetAll()
                 .Select(forum => new ForumListingModel {
                     Id = forum.Id,
                     Name = forum.Title,
                     Description = forum.Description,
                     ImageUrl=forum.ImageUrl
                 });

            var model = new ForumIndexModel()
            {
                ForumListing = forums
            };

            return View(model);
        }

        public IActionResult Topic(int id, string searchQuery)
        {
            var forum = _forumservice.GetByid(id);

            //var posts = _postservice.GetPostsByForum(id);

            var posts = new List<Post>();
           
         
            posts = _postservice.GetFilteredPosts(forum, searchQuery).ToList();
            
             //posts = forum.Posts.ToList();

            var postListings = posts.Select(post => new PostListModel
            {
                Id=post.Id,
                AuthorId=post.User.Id,
                AuthorRating=post.User.Rating,
                AuthorName=post.User.UserName,
                Title=post.Title,
                DatePosted=post.Created.ToString(),
                RepliesCount=post.Replies.Count(),
                //Forum = new ForumListingModel
                //{
                //    Id=forum.Id,
                //    Name=forum.Title,
                //    Description=forum.Description,
                //    ImageUrl=forum.ImageUrl
                //}
                Forum = BuildForumListing(post.Forum)
            });

            var model = new ForumTopicModel()
            {
                 Posts=postListings,
                 Forum= BuildForumListing(forum)

            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Search(int id,string searchQuery)
        {
            return RedirectToAction("Topic", new { id, searchQuery });
        }

        [HttpGet]
        [Authorize(Roles ="Admin")]
        public IActionResult Create()
        {

            var model = new AddForumModel();
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(AddForumModel model)
        {
            if (ModelState.IsValid)
            {
                var forum = new Forum()
                {
                    Title=model.Title,
                    Description=model.Description,
                    Created=DateTime.Now
                };

                if (model.ImageUpload != null && model.ImageUpload.Length > 0)
                {
                    var uploadDir = @"images/forum";
                    var fileName = Path.GetFileNameWithoutExtension(model.ImageUpload.FileName);
                    var extension = Path.GetExtension(model.ImageUpload.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = DateTime.UtcNow.ToString("yymmssfff") + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await model.ImageUpload.CopyToAsync(new FileStream(path, FileMode.Create));
                    forum.ImageUrl = "/" + uploadDir + "/" + fileName;
                }

                await _forumservice.Create(forum);
                return RedirectToAction("Index", "Forum");
            }
            return View(model);
        }

        //private ForumListingModel BuildForumListing(Post post)
        //{
        //    var forum = post.Forum;
        //    return BuildForumListing(forum);
        //}

        private ForumListingModel BuildForumListing(Forum forum)
        {
            return new ForumListingModel
            {
                Id = forum.Id,
                Name=forum.Title,
                Description=forum.Description,
                ImageUrl=forum.ImageUrl
            };
        }

       
    }
}
