﻿using Blogforum.Data;
using Blogforum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogforum.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private readonly IPost _postService;
        private readonly IForum _forumService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IApplicationUser _userService;

        public PostController(IPost postService,IForum forumService, UserManager<ApplicationUser> userManager, IApplicationUser userService)
        {
            _postService = postService;
            _forumService = forumService;
            _userManager = userManager;
            _userService = userService;
        }
        public IActionResult Index(int id)
        {
            var post = _postService.GetById(id);
            var replies = BuildPostReplies(post.Replies);
            var model = new PostIndexModel
            {
                Id=post.Id,
                Title=post.Title,
                AuthorId=post.User.Id,
                AuthorName=post.User.UserName,
                AuthorImageUrl=post.User.ProfileImageUrl,
                AuthorRating=post.User.Rating,
                Created=post.Created,
                PostContent=post.Content,
                Replies = replies,
                ForumId=post.Forum.Id,
                ForumName=post.Forum.Title,
                IsAuthorAdmin= IsAuthorAdmin(post.User)

            };
            return View(model);
        }

        public IActionResult Create(int id)
        {
            //This is the forum id


            var forum = _forumService.GetByid(id);

            var model = new NewPostModel()
            {
                ForumName=forum.Title,
                ForumId=forum.Id,
                ForumImageUrl=forum.ImageUrl,
                AuthorName=User.Identity.Name
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Addpost(NewPostModel model)
        {
            var userId = _userManager.GetUserId(User);
            var use= _userManager.GetUserId(User);
            var s = Convert.ToInt32(use);
            var user = await _userManager.FindByIdAsync(userId);
            var post = BuildPost(model, user);

            await _postService.Add(post);
            await _userService.UpdateUserRating(s, typeof(Post));

            return RedirectToAction("Index", "Post", new { id= post.Id });
        }

        private Post BuildPost(NewPostModel model, ApplicationUser user)
        {
            var forum = _forumService.GetByid(model.ForumId);
            return new Post()
            {
                Title = model.Title,
                Content = model.Content,
                Created = DateTime.Now,
                User = user,
                Forum=forum
            };
        }

        private IEnumerable<PostReplyModel> BuildPostReplies(IEnumerable<PostReply> replies)
        {
            return replies.Select(reply => new PostReplyModel
            {
                Id=reply.Id,
                AuthorName=reply.User.UserName,
                AuthorId=reply.User.Id,
                AuthorImageUrl=reply.User.ProfileImageUrl,
                AuthorRating=reply.User.Rating,
                Created=reply.Created,
                ReplyContent=reply.Content,
                IsAuthorAdmin=IsAuthorAdmin(reply.User)
            });
        }


        private bool IsAuthorAdmin(ApplicationUser user)
        {
            return _userManager.GetRolesAsync(user).Result.Contains("Admin");
        }

    }
}
