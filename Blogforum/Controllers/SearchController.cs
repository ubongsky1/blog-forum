﻿using Blogforum.Data;
using Blogforum.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogforum.Controllers
{
    public class SearchController : Controller
    {
        private readonly IPost _postService;

        public SearchController(IPost postService)
        {
            _postService = postService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Results(string searchQuery)
        {
            var post = _postService.GetFilteredPosts(searchQuery);

            var areNoResults = (!string.IsNullOrEmpty(searchQuery) && !post.Any());

            var postListings = post.Select(posts => new PostListModel
            {
                Id=posts.Id,
                AuthorId=posts.User.Id,
                AuthorName=posts.User.UserName,
                AuthorRating=posts.User.Rating,
                Title=posts.Title,
                DatePosted=posts.Created.ToString(),
                RepliesCount=posts.Replies.Count(),
                Forum=BuildForumListing(posts.Forum)
            });


            var model = new SearchResultModel
            {
                Posts=postListings,
                SearchQuery=searchQuery,
                EmptySearchResults= areNoResults
            };

            return View(model);
        }

        private ForumListingModel BuildForumListing(Forum forum)
        {
            return new ForumListingModel
            {
                Id=forum.Id,
                ImageUrl=forum.ImageUrl,
                Name=forum.Title,
                Description=forum.Description
            };
        }

        public IActionResult Search(string searchQuery)
        {
            return RedirectToAction("Results", new { searchQuery });
        }
    }
}
