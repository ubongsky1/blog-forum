﻿using Blogforum.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Blogforum.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IApplicationUser _userService;
        private readonly IUpload _uploadService;
        private readonly HostingEnvironment _hostingEnvironment;

        public ProfileController(UserManager<ApplicationUser> userManager,
                                IApplicationUser userService,IUpload uploadService, HostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _userService = userService;
            _uploadService = uploadService;
            _hostingEnvironment = hostingEnvironment;
        }

        [Authorize(Roles ="Admin")]
        public IActionResult Index()
        {
            var profiles = _userService.GetAll().OrderBy(user=>user.Rating)
                           .Select(u=> new ProfileModel
                           {
                              Email=u.Email,
                              UserName=u.UserName,
                              ProfileImageUrl=u.ProfileImageUrl,
                              UserRating=u.Rating,
                              MemberSince=u.MemberSince,

                           });

            var model = new ProfileListModel()
            {
                Profiles=profiles
            };
            return View(model);
        }

        public IActionResult Detail(int id)
        {
            var s = _userService.GetById(id);

            var model = new ProfileModel()
            {
                UserId=s.Id,
                UserName=s.UserName,
                UserRating=s.Rating,
                Email=s.Email,
                ProfileImageUrl=s.ProfileImageUrl,
                MemberSince=s.MemberSince
            };


            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UploadProfileImage(IFormFile File)
        {

            var userId = await _userManager.GetUserAsync(User);


            if (ModelState.IsValid)
            {
                if (File != null && File.Length > 0)
                {
                    var uploadDir = @"images/ProfilePicx";
                    var fileName = Path.GetFileNameWithoutExtension(File.FileName);
                    var extension = Path.GetExtension(File.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = DateTime.UtcNow.ToString("yymmssfff") + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await File.CopyToAsync(new FileStream(path, FileMode.Create));
                    userId.ProfileImageUrl = "/" + uploadDir + "/" + fileName;
                }

                await _userService.UpdateUser(userId);

                return RedirectToAction("Detail", "Profile", new { id = userId.Id });
            }

            return View();

           
        }


    
    }
}
