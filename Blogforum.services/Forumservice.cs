﻿using Blogforum.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blogforum.services
{
    public class Forumservice : IForum
    {
        private readonly ApplicationDbContext _context;

        public Forumservice(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Create(Forum forum)
        {
            _context.Add(forum);
            await _context.SaveChangesAsync();
        }

        public Task Delete(int forumid)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ApplicationUser> GetActiveUsers()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Forum> GetAll()
        {
            return _context.Forums.Include(forum => forum.Posts);
        }

        public Forum GetByid(int id)
        {
            var forum = _context.Forums.Where(f => f.Id == id)
                 .Include(f => f.Posts)
                 .ThenInclude(p => p.User)
                 .Include(f => f.Posts)
                 .ThenInclude(f => f.Replies)
                 .ThenInclude(r=>r.User).FirstOrDefault();

            return forum;
        }

        public Task UpdateForumDescription(int forumid, string newDescription)
        {
            throw new NotImplementedException();
        }

        public Task UpdateForumTitle(int forumid, string newTitle)
        {
            throw new NotImplementedException();
        }
    }
}
