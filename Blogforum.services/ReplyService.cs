﻿using Blogforum.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blogforum.services
{
    public class ReplyService : IReplyService
    {
        private readonly ApplicationDbContext _context;

        public ReplyService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Create(PostReply reply)
        {
            _context.Add(reply);
            await _context.SaveChangesAsync();
        }

        public Task DeleteById(int id)
        {
            throw new NotImplementedException();
        }

        public PostReply GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PostReply> GetReplies()
        {
            throw new NotImplementedException();
        }

        public Task Update(PostReply reply)
        {
            throw new NotImplementedException();
        }
    }
}
