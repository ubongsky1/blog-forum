﻿using Blogforum.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blogforum.services
{
    public class ApplicationUserService : IApplicationUser
    {
        private readonly ApplicationDbContext _context;

        public ApplicationUserService(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<ApplicationUser> GetAll()
        {
            return _context.Users;
        }

        public ApplicationUser GetById(int id)
        {
            return
            _context.Users.FirstOrDefault(userid => userid.Id==id);
        }

        public async Task UpdateUserRating(int userId, Type type)
        {
            var user = GetById(userId);
            user.Rating = CalculteUserRating(type,user.Rating);
           
            await _context.SaveChangesAsync();
        }

        private int CalculteUserRating(Type type, int rating)
        {
            var inc = 0;

            if (type==typeof(Post))
            {
                inc = 1;
            }

            if (type == typeof(PostReply))
            {
                inc = 3;
            }

            return rating + inc;
        }

        public Task SetProfileImage(string id, Uri uri)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateUser(ApplicationUser user)
        {

            _context.Update(user);
            await _context.SaveChangesAsync();
           
        }

        public Task IncrementRating(int userId, Type type)
        {
            throw new NotImplementedException();
        }
    }
}
