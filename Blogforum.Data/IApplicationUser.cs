﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blogforum.Data
{
   public interface IApplicationUser
    {

        ApplicationUser GetById(int id);

        IEnumerable<ApplicationUser> GetAll();

        Task SetProfileImage(string id, Uri uri);
        Task IncrementRating(int userId, Type type);
        Task UpdateUserRating(int userId, Type type);

        Task UpdateUser(ApplicationUser user);
    }
}
