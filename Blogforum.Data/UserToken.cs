﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blogforum.Data
{
    public class UserToken : IdentityUserToken<int>
    {
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
