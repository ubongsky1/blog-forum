﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blogforum.Data
{
  public  interface IReplyService
    {
        Task Create(PostReply reply);
        IEnumerable<PostReply> GetReplies();
        PostReply GetById(int id);
        Task Update(PostReply reply);
        Task DeleteById(int id);
    }
}
