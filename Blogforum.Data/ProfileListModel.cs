﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blogforum.Data
{
   public class ProfileListModel
    {
        public IEnumerable<ProfileModel> Profiles { get; set; }
    }
}
