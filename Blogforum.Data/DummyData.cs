﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blogforum.Data
{
    public class DummyData
    {
        public static async Task Initialize(ApplicationDbContext context,
                              UserManager<ApplicationUser> userManager,
                              RoleManager<ApplicationRole> roleManager)
        {
            context.Database.EnsureCreated();

            int adminId1 = 0;
            int adminId2 = 0;
            int adminId3 = 0;
            int adminId4 = 0;
            int adminId5 = 0;
            int adminId6 = 0;
            int adminId7 = 0;
            int adminId8 = 0;



            string role1 = "Admin";
            string desc1 = "This is the administrators role";

            string role2 = "User";
            string desc2 = "This is the users role";
 
            string role8 = "SuperAdmin";
            string desc8 = "This is the Super Admin role";



            string password = "P@$$w0rd";

            if (await roleManager.FindByNameAsync(role1) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role1, desc1, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role2) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role2, desc2, DateTime.Now));
            }
          
            if (await roleManager.FindByNameAsync(role8) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role8, desc8, DateTime.Now));
            }
            if (await userManager.FindByNameAsync("ubongsky1@gmail.com") == null)
            {
                var user = new ApplicationUser
                {

                    UserName = "ubongsky1@gmail.com",
                    Email = "ubongsky1@gmail.com",
                    PhoneNumber = "07082162958",
                    FirstName = "Ubong",
                    LastName = "Umoh"

                };


                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role1);
                }
                adminId1 = user.Id;
            }




            if (await userManager.FindByNameAsync("akinbamidelet@gmail.com") == null)
            {
                var user = new ApplicationUser
                {

                    UserName = "akinbamidelet@gmail.com",
                    Email = "akinbamidelet@gmail.com",
                    PhoneNumber = "090123456789",
                    FirstName = "Admin",
                    LastName = "Test"


                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role2);
                }
                adminId2 = user.Id;
            }




        }
    }
}
